<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use File;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $contents = Storage::disk('public')->get('1232/1232');

        $xml = simplexml_load_string($contents);
        $json = json_encode($xml);
        $cars = json_decode($json,TRUE);

        //dd($cars);

        return view('home', compact('cars'));
    }



    public function images($vehicleId)
    {

        $initialImages = Storage::disk('public')->files('/1232');
        
        //https://stackoverflow.com/questions/12315536/search-for-php-array-element-containing-string

        $searchword = $vehicleId;
        $images = array_filter($initialImages, function($var) use ($searchword) { return preg_match("/\b$searchword\b/i", $var); });

        return view('images', compact('images'));
    }


}
