@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">List</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   {{--  {{ $cars['Vehicle'][0]['VehicleId'] }} --}}

                    
                        
                    <div class="table-responsive">   
                      <table class="table table-striped">
                        <thead>
                          <tr class="table-dark">
                            <th scope="col">Car</th>
                            <th scope="col">Manufacturer</th>
                            <th scope="col">Model</th>
                            <th scope="col">Year</th>
                            <th scope="col">VIN</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($cars['Vehicle'] as $car)
                          <tr>
                            <td><a href="{{ route('images', $car['VehicleId']) }}"><img src="{{ asset('storage/1232').'/'.$car['VehicleId'] . '-1.jpg' }}" width="100px;" height="100px;" alt="img"></a></td>
                            <td>{{ $car['Manufacturer'] }}</td>
                            <td>{{ $car['Model'] }}</td>                            
                            <td>{{ $car['ManuYear'] }}</td>
                            @if($car['VIN'])<td>{{ $car['VIN'] }}</td>@endif
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>

                    <div>
                    AUTOBOX (WAIRAU &amp; DIANA BRANCHES)<br>
                    <br>
                    ** MAIN OFFICE (192 WAIRAU RD, WAIRAU VALLEY)<br>
                    ** CLOSED ON SUNDAY **<br>
                    <br>
                    ** SPECIAL NOTE **<br>
                    WE HAVE TWO OFFICES ON WAIRAU VALLEY,<br>
                    PLEASE GIVE US CALL OR TXT TO CHECK THE LOCATION OF VEHICLE BEFORE YOU VISIT OUR OFFICES.<br>
                    <br>
                    ** AUTOBOX **<br>
                    Ben / Sales Manager   021-043-5921<br>
                    Hyun / Sales                020-4161-0298<br>
                    Julia / Office                09-390-0050<br>
                    Sam / Finance             022-192-1169<br>
                    <br>
                    ** On Road Cost (ORC) is an extra. <br>
                    ** Extremely Tidy Vehicle Ractis 2009 X_HID Selection<br>
                    ** HID headlights<br>
                    ** Good condition of 4 Goodyear Tyres<br>
                    ** ONLY 46,000kms / Low kms<br>
                    ** AA Odometer Certified<br>
                    ** Enough Space for 5 People<br>
                    ** Remote Key<br>
                    <br>
                    If you would like a test drive of this vehicle, please do contact our friendly team at &apos;AUTOBOX&apos; via our contact details below!<br>
                    <br>
                    About AUTOBOX  <br>
                    <br>
                    Based in Henderson and North Shore, we specialize in selling pre-loved cars to the whole of New Zealand. Our aim is to help you drive away with the second hand car that suits your needs both practically and financially. We offer some of the most competitive deals on used cars and across a variety of car brands. We have a great selection of used cars in a range of makes and models, from family cars to small hatchbacks, we are confident that we can find the right car for you. <br>
                    <br>
                    --  Trade-Ins** <br>
                    --   Odometer certificate for fresh import vehicle** <br>
                    --  Quick and easy Finance available<br>
                    --  Zero Deposit options** <br>
                    --  Nationwide delivery** <br>
                    --  Servicing and Parts** <br>
                    --  Vehicle valuations<br>
                    --  Mechanical warranty** <br>    
                    --  AA or Third part inspection**<br>
                    <br>
                    TEAM @ AUTOBOX<br>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
